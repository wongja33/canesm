#!/bin/bash
#
#   SETUP CI
#
#       This script sets up a run using setup-canesm,
#       and then configures the settings (via config-canesm)
#       after making the necessary config changes. It
#       then saves the restart files in prep for the run
#       stage.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set -ex

# source global ci config vars
source ./tests/ci/ci.conf

# check for necessary config vars and set downstream ones
[[ -z $REPO_PATH ]]             && { echo "REPO_PATH must be defined"; exit 1; }
[[ -z $IDENTIFIER ]]            && { echo "IDENTIFIER must be defined"; exit 1; }
[[ -z $RUN_CONFIG ]]            && { echo "RUN_CONFIG must be defined"; exit 1; }
[[ -z $RUNMODE ]]               && { echo "RUNMODE must be defined"; exit 1; }
[[ -z $START_YEAR ]]            && { echo "START_YEAR must be defined"; exit 1; }
[[ -z $RESTART_RUNID ]]         && { echo "RESTART_RUNID must be defined"; exit 1; }
[[ -z $RESTART_BRANCH_TIME ]]   && { echo "RESTART_BRANCH_TIME must be defined"; exit 1; }
[[ -z $COMPUTE_SYSTEM ]]        && { echo "COMPUTE_SYSTEM must be defined"; exit 1; }
[[ -z $TEST_RUN_DIR ]]          && { echo "TEST_RUN_DIR must be definied"; exit 1; }
RUNID=ci-${IDENTIFIER}-${CI_COMMIT_SHA:0:7}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# setup the test run's directory and source the environment
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mkdir -p $TEST_RUN_DIR
cd $TEST_RUN_DIR
echo ""
echo "Setting up ${RUN_CONFIG} run, ${RUNID}..."
echo ""
setup-canesm runid=${RUNID} config=${RUN_CONFIG} repo=${REPO_PATH} fetch_method=copy
cd $RUNID

# turn off set -x output while environment is sourced as it 
# will fill up the CI terminal
source env_setup_file 2> /dev/null

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# set necessary configuration settings and then config the test run
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#   Notes:
#       - we set run_start|stop_time and start|stop_time to be START_YEAR
#           to get `config-canesm` to execute. In the run, we manually set
#           the job stop date to execute a much shorter duration
echo ""
echo "Configuring test run..."
echo ""
sed -i "s/^compute_system=.*/compute_system=${COMPUTE_SYSTEM}/" canesm.cfg
sed -i "s/^run_start_time=.*/run_start_time=${START_YEAR}/" canesm.cfg
sed -i "s/^run_stop_time=.*/run_stop_time=${START_YEAR}/" canesm.cfg
sed -i "s/^start_time=.*/start_time=${START_YEAR}/" canesm.cfg
sed -i "s/^stop_time=.*/stop_time=${START_YEAR}/" canesm.cfg
sed -i "s/^runmode=.*/runmode=${RUNMODE}/" canesm.cfg
sed -i "s/^parent_runid=.*/parent_runid=${RESTART_RUNID}/" canesm.cfg
sed -i "s/^parent_branch_time=.*/parent_branch_time=${RESTART_BRANCH_TIME}/" canesm.cfg
config-canesm

#~~~~~~~~~~~~~~~~~~
# save the restarts
#~~~~~~~~~~~~~~~~~~
echo ""
echo "Saving input restarts..."
echo ""
save_restart_files

echo ""
echo "Setup successful!"
echo ""
