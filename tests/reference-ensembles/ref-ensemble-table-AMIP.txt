runid           restart_files            restart_dates   tapeload    canesm_cfg:run_start_time   canesm_cfg:run_stop_time    start_time      stop_time   canesm_cfg:runmode          canesm_cfg:months
adev-REF        restart-canesm5d1rc1-01     0001_m12        False       2003                        2003                      2003_m01        2003_m12    CanAM-Dev-2003-2008         12
mam-adev-REF    maminit-v510-26d1           0001_m12        False       2003                        2003                      2003_m01        2003_m12    CanAM-MAM-Dev-2003-2008     6

# To be tested occasionally. This runmode currently leaves ~1TB on disk and takes 8+ hours to run.
#pam-adev-REF    p2-amip-ru01                2002_m12        False       2003                        2003                      2003_m01        2003_m12    CanAM-PAM                   2
