from abc import ABC, abstractmethod
import os
import shutil
import subprocess
import filecmp

class experiment(ABC):
  """Holds information about a given experiment
  """

  name = None
  # Name within the master data directgory containing the experiment's
  # input files
  data_directory_name = None

  # Where the particular experiment was run
  run_directory = None

  # Create dictionaries storing the locations of the executables
  built_exes = {
    'atmosphere' : None,
    'coupler'    : None,
    'ocean'      : None
  }
  cached_exes = {
    'atmosphere' : None,
    'coupler'    : None,
    'ocean'      : None
  }

  # List the files containing component checksums
  checksum_files = []

  # Contains files that contain runtime-configurable options
  modifiable_files = {}

  # Define the run command for the experiment
  run_command = None

  def setup_run_directory(self, platform):
    source_data = f'{platform.data_directory}/{self.data_directory_name}'
    files = os.listdir(source_data)
    if os.path.exists(self.run_directory):
      shutil.rmtree(self.run_directory)
    os.makedirs(self.run_directory,exist_ok=True)
    for file in files:
      basename = os.path.basename(file)
      if basename in self.modifiable_files.keys():
        shutil.copy(f'{source_data}/{file}',self.run_directory)
      else:
        os.symlink(f'{source_data}/{file}', f'{self.run_directory}/{basename}')

  def configure(self):
    """
    Configure the experiment

    Modifies any files that contan runtime options by looping through
    the modifiable_files dictionary that was set during hte creation of
    the experiment object
    """

    for file, values in self.modifiable_files.items():
      for key, replacement in values.items():
        value = replacement['replacement']
        subprocess.run(
          [f"sed -i 's/{key}/{value}/' {self.run_directory}/{file}"],
          shell=True
        )

  def integrate(self, platform, num_atmosphere_nodes, num_ocean_ranks):
    """Integrate the previously configured experiment

    :param platform: The platform on which this experiment is to be run
    :param num_atmosphere_nodes: Number of nodes for the AGCM
    :param num_ocean_ranks: Number of MPI ranks of the ocean
    :raises ExperimentError: Raised if integration failed
    """
    for key, value in platform.env_vars_run.items():
        os.environ[key] = value

    if self.is_coupled:
        run_command = self.run_command.format(
          agcm_exe = self.cached_exes['atmosphere'],
          cpl_exe  = self.cached_exes['coupler'],
          ocn_exe  = self.cached_exes['ocean'],
          num_atm_ranks = num_atmosphere_nodes,
          cpl_ranks = 1,
          ocn_ranks = num_ocean_ranks
        )
    output = subprocess.run(
      [f'{platform.unlimit_stacksize_cmd}; {run_command}'],
      cwd=self.run_directory,
      capture_output=True,
      shell=True,
      text=True)

    try:
      output.check_returncode()
      print(f'Experiment {self.name} completed successfully', flush=True)
    except:
      print(output.stdout)
      print(output.stderr, flush=True)
      raise IntegrationError(f'Experiment {self.name} FAILED')

  def test_regression(self, platform):
    check_regression = {}
    for checksum_file in self.checksum_files:
      check_regression[checksum_file] = not filecmp.cmp(
        f'{self.run_directory}/{checksum_file}',
        f'{platform.canesm_source}/external-ci/answers/{platform.name}/{checksum_file}'
      )

    if any(check_regression.values()):
      for checksum_file, regression in self.checksum_files.items():
        if regression:
          with open(f'{self.run_directory}/{checksum_file}') as f:
            lines = f.readlines()
          print(f'{checksum_file}: REGRESSION')
          print(lines)
      raise RegressionError(f'Answers changed in {self.name}')

class coupled_experiment(experiment):
  is_coupled = True
  atmosphere_sizes_file = None
  atmosphere_config_file = None
  ocean_config = 'CCC_CANCPL_ORCA1_LIM_CMOC'
  run_command = (
    'mpiexec -n {num_atm_ranks} {agcm_exe} : '
    '-n {cpl_ranks} {cpl_exe} : '
    '-n {ocn_ranks} {ocn_exe}'
  )

  checksum_files = [
    'final.state',
    'agcm.final.state'
  ]

  # Choose all the files that can be modified. The nested directory specifies
  # the patterns to replace.
  modifiable_files = {
    'modl.dat':{
      '194472000':{
        'description':'KFINAL: the final timestep of the atmosphere (6 hours)',
        'replacement':'194436984'
      }
    },
    'namelist':{
      'nn_itend.*':{
        'description':'The final timestep of the ocean model (6 hours)',
        'replacement':'nn_itend = 6'
      }
    }
  }

  def __init__(self, cached_dir, platform):
    self.built_exes['atmosphere'] = f'{platform.atmosphere_source}/build/bin/canam.exe.{self.name}'
    self.built_exes['coupler'] = f'{platform.coupler_source}/build/cancpl.exe.{self.name}'
    self.built_exes['ocean'] = f'{platform.ocean_source}/nemo/CONFIG/{self.ocean_config}/nemo.exe.{self.name}'
    self.cached_exes['atmosphere'] = f'{cached_dir}/canam.exe.{self.name}'
    self.cached_exes['coupler'] = f'{cached_dir}/cancpl.exe.{self.name}'
    self.cached_exes['ocean'] = f'{cached_dir}/nemo.exe.{self.name}'
    self.run_directory = f'{platform.temp_directory}/{self.data_directory_name}'

class piControl(coupled_experiment):
  name = 'piControl'
  atmosphere_sizes_file = 'build/include/.defaults/cppdef_sizes.h'
  atmosphere_config_file = 'build/include/.defaults/cppdef_config.h'
  data_directory_name = 'canesm5_piControl_config'

  def __init__(self, cached_dir, platform):
    self.atmosphere_sizes_file = f'{platform.atmosphere_source}/{self.atmosphere_sizes_file}'
    self.atmosphere_config_file = f'{platform.atmosphere_source}/{self.atmosphere_config_file}'
    super().__init__(cached_dir, platform)

class IntegrationError( Exception ):
  """Raised when an experiment fails to integrate
  """


experiments = {
  'piControl':piControl
}
