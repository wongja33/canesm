# MKMF template for the intel compiler 
#=====================================
# Define compiler and linker
FC := mpif90
LD := mpif90

# Set potential optimization levels
## OPT:   The fastest possible
## DEBUG: Enable debug-compatible flags
## REPRO: Ensures reproducibility while retaining some speed
OPT   ?= 0
DEBUG ?= 0
REPRO ?= 0

# By default, choose the fastest settings to compile
ifeq ($(OPT),0)
ifeq ($(DEBUG),0)
ifeq ($(REPRO),0)
OPT = 1
endif
endif
endif

# Set additional compilation options
# COMPILE_32BIT:        1 => compile with 64bit          (default off)
# COMPILE_WITH_ESMF:    1 => compile with ESMF libraries (default off)
# COMPILE_WITH_OPENMP:  1 => compile with openmp support (default on)
COMPILE_32BIT       ?= 0
COMPILE_WITH_ESMF   ?= 0
COMPILE_WITH_OPENMP ?= 1

# FFLAGS
#-------
FFLAGS_DEFAULT  = -traceback -qmkl -fpp -Qoption,link,--noinhibit-exec -list -static-intel
FFLAGS_DEFAULT += -assume buffered_stdout,protect_parens -convert big_endian -threads -DIntelFTN $(shell nf-config --fflags)
FFLAGS = $(FFLAGS_DEFAULT)
FFLAGS_64BIT = -r8 -i8 -mcmodel=medium -shared-intel
FFLAGS_OPENMP = -qopenmp

# Openmp support
ifeq ($(COMPILE_WITH_OPENMP),1)
FFLAGS += $(FFLAGS_OPENMP)
endif

# Set the options for various levels of optimizations
FFLAGS_OPT  = -O2 -mp1 -xCORE-AVX512 -align array64byte
FFLAGS_REPRO = -O2 -fp-model precise
FFLAGS_DEBUG = -O0 -g

# Optimization flags
ifeq ($(OPT),1)
FFLAGS += $(FFLAGS_OPT)
endif
ifeq ($(REPRO),1)
FFLAGS += $(FFLAGS_REPRO)
endif
ifeq ($(DEBUG),1)
FFLAGS += $(FFLAGS_DEBUG)
endif

# 64-bit flags if requested
ifeq ($(COMPILE_32BIT),0)
FFLAGS += $(FFLAGS_64BIT)
endif

# INCLUDE AND LINK FLAGS
#-----------------------
ifeq ($(COMPILE_WITH_ESMF),1)
ifndef CANESM_ESMF_INSTALL_DIR
$(error CANESM_ESMF_INSTALL_DIR must be set to compile with ESMF!)
endif
endif

# INCLUDE FLAGS
INCLUDE_FLAGS_DEFAULT = -I./
INCLUDE_FLAGS_ESMF = -I${CANESM_ESMF_INSTALL_DIR}/mod -I${CANESM_ESMF_INSTALL_DIR}/include
INCLUDE_FLAGS = $(INCLUDE_FLAGS_DEFAULT)
ifeq ($(COMPILE_WITH_ESMF),1)
INCLUDE_FLAGS += $(INCLUDE_FLAGS_ESMF)
endif

# append to FFLAGS
FFLAGS += $(INCLUDE_FLAGS)

# LINK FLAGS
LDFLAGS_OPENMP = -qopenmp
LDFLAGS_ESMF = -L${CANESM_ESMF_INSTALL_DIR}/lib -lesmf
LDFLAGS_NETCDF = $(shell nf-config --flibs)
LDFLAGS_DEFAULT = -mkl $(LDFLAGS_OPENMP) $(LDFLAGS_NETCDF)
LDFLAGS = $(LDFLAGS_DEFAULT) 
ifeq ($(COMPILE_WITH_ESMF),1)
# use default ESMF library
LDFLAGS += $(LDFLAGS_ESMF)
endif
