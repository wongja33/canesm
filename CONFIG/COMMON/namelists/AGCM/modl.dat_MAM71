! The following variables are related to the "critical parameters" for the CanAM
! Only the scalars are listed here.  The lists, e.g., level information, are listed
! at the end of the file since they can get pretty long.

! Variables read in by ingcm
&INGCM
  ksteps=288,       ! number of time-steps between coupling cycles
  kstart=0,         ! final step counter for the previous run
  kfinal=999,       ! ending step counter for the AGCM
  agcm_rstctl=0,    ! controls how ingcm handles kstart comparison between the restart and this namelist
                    !   0 -> just outputs the input variables
                    !   1 -> if bails if there are inconsistencies
  newrun=0,
  levs1=71,         ! number of vertical moisture levels; for initialization runs (kount=0 only)

  incd=1,
  jlatpr=0,
  delt=300.0,       ! AGCM time-step
  ifdiff=0,
  icoordc="ET15",
  iyear=1850,
/

&agcm_sizes
  nlatd   = 96,         ! number of gaussian latitudes on the dynamics grid
  lonsld  = 192,        ! number of longitudes on the dynamics grid
  lond    = 192         ! number of grid points per slice for dynamics
  nlat    = 64,         ! number of gaussian latitudes on the physics grid
  lonsl   = 128,        ! number of longitudes on the physics grid
  lonp    = 128,        ! number of grid points per slice for physics
  ilev    = 71,         ! number of vertical model levels
  levs    = 71,         ! number of vertical moisture levels
  nnodex  = 32,         ! number of (smp?) nodes used to run the AGCM
  ilg     = 131
  ilgd    = 195
  lmtotal = 64
/
!  ntrac=16,             ! total number of tracers in the AGCM
!  ntraca=12,            ! number of advected tracers in the AGCM

! Other critical scalar parameters need to define the structure of CanAM
&CRIT_PARAM_CANAM_SCALAR
  lmt=63,               ! spectral truncation wave number
  lon=128,              ! number of grid points per slice for physics
  lay=2,
  coord=ET15,
  plid=.0575,
  iflm=1,               ! integer switch to control fractional land mask (1 -> on)
  nnode_a=32,           ! number of (smp?) nodes used to run the AGCM - this doesn't seem to be the right name.. it gets changed to NNODEX in the code
  NRFP=1
/

&agcm_options
  agcm_river_routing = .true.
  biogeochem = .true.
  cf_sites = .false.
  dynrhs = .false.
  isavdts = .false.
  myrssti = .false.
  parallel_io = .true.
  relax = .false.
  timers = .false.
  use_canam_32bit = .true.
  rcm = .false.
/

! Parameters for relaxation scheme
&relax_parm
      !--- Set the relaxation parameters
      relax_spc = 1             ! spectral nudging
      relax_spc_bc = 0          ! spectral climatological nudging

      relax_tg1 = 0             ! Ground temperature level 1 nudging
      relax_tg1_bc = 0          ! Ground temperature level 1 climatological nudging

      relax_wg1 = 0             ! Ground moisture level 1 nudging
      relax_wg1_bc = 0          ! Ground moisture level 1 climatological nudging

      relax_ocn = 0             ! Upper ocean (SST/SIC/SICN) nudging
      relax_ocn_bc = 0          ! Upper ocean climatological nudging

      iperiod_spc=6             ! Spectral nudging cycle in hours.
      iperiod_lnd=24            ! Land nudging cycle in hours.
      iperiod_lnd_clm=6         ! Land climatological nudging cycle in hours.
      iperiod_ocn=24            ! Ocean nudging cycle in hours.
      iperiod_ocn_clm=24        ! Ocean climatological nudging cycle in hours.

      relax_p=1.                ! VORT vorticity relaxation switch.
      relax_c=1.                ! DIV  divergence relaxation switch.
      relax_t=1.                ! TEMP temperature relaxation switch.
      relax_es=1.               ! ES   humidity relaxation switch.
      relax_ps=0.               ! PS   ln(surface pressure) relaxation switch.

      tauh_p=24.                ! VORT relaxation time scale in hours
      tauh_c=24.                ! DIV  relaxation time scale in hours
      tauh_t=24.                ! TEMP relaxation time scale in hours
      tauh_es=24.               ! HUM  relaxation time scale in hours
      tauh_ps=24.               ! PS   relaxation time scale in hours

      tauh_tg1=72.              ! TG1 relaxation time scale in hours
      tauh_wg1=72.              ! WG1 relaxation time scale in hours

      ntrunc_p=21               ! truncation of VORT tendencies
      ntrunc_c=21               ! truncation of DIV  tendencies
      ntrunc_t=21               ! truncation of TEMP tendencies
      ntrunc_es=21              ! truncation of HUM  tendencies
      ntrunc_ps=21              ! truncation of PS   tendencies

      itrunc_tnd = 1            ! itrunc_tnd - (0/1) truncate tendency with truncf
      itrunc_anl = 0            ! trunc_anl - (0/1) truncate analysis data towards which model is nudged

      ivfp=0                    ! =0, no vertical profile to nudge everywhere with equal strength
                                ! =1, use vertical profile at the top to reduce nudging near the top
                                ! =2 vertical profile at the bottom to reduce nudging near the bottom
                                ! =3 vertical profile both at the top and bottom to reduce nudging at both ends

      sgtop=0.001               ! top full levels (0.001~1hPa)
      shtop=0.001               ! top half levels (0.001~1hPa)

      sgfullt_p=0.010           ! where to start reducing forcing (0.010~10hPa)
      sgfullt_c=0.010           ! where to start reducing forcing (0.010~10hPa)
      shfullt_t=0.010           ! where to start reducing forcing (0.010~10hPa)
      shfullt_e=0.010           ! where to start reducing forcing (0.010~10hPa)

      sgdampt_p=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
      sgdampt_c=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
      shdampt_t=0.010           ! reduction factor from shfull to shtop (0.01=1%)
      shdampt_e=0.010           ! reduction factor from shfull to shtop (0.01=1%)

      sgbot=0.995               ! bottom full levels (0.995~995hPa)
      shbot=0.995               ! bottom half levels (0.995~995hPa)
      sgfullb_p=0.850           ! where to start reducing forcing (0.850~850hPa)
      sgfullb_c=0.850           ! where to start reducing forcing (0.850~850hPa)
      shfullb_t=0.850           ! where to start reducing forcing (0.850~850hPa)
      shfullb_e=0.850           ! where to start reducing forcing (0.850~850hPa)

      sgdampb_p=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
      sgdampb_c=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
      shdampb_t=0.010           ! reduction factor from shfulb to shbot (0.01=1%)
      shdampb_e=0.010           ! reduction factor from shfulb to shbot (0.01=1%)

      tauh_sst=72.              ! in hours - nudging time scale for SSTs
      tauh_edge=72.             ! in hours - nudging time scale for sea-ice mass (SIC) near edge
                                ! may use shorter time scale since it is better observed
      tauh_sic=168.             ! in hours - nudging time scale for sea-ice mass (SIC)

      rlx_sst=1.                ! convert delta sst to heat tendency
      rlx_sic=1.                ! convert delta sic to heat tendency

      rlx_tbeg=1.               ! turn on/off (1/0) tbeg heat tendency
      rlx_tbei=1.               ! turn on/off (1/0) tbei heat tendency
      rlx_tbes=1.               ! turn on/off (1/0) tbes heat tendency

      rlc_tbeg=0.               ! turn on/off (1/0) tbeg heat clim. tendency
      rlc_tbei=0.               ! turn on/off (1/0) tbei heat clim. tendency
      rlc_tbes=0.               ! turn on/off (1/0) tbes heat clim. tendency

      rlx_sbeg=1.               ! turn on/off (1/0) sbeg heat tendency
      rlx_sbei=1.               ! turn on/off (1/0) sbei heat tendency
      rlx_sbes=1.               ! turn on/off (1/0) sbes heat tendency

      rlc_sbeg=0.               ! turn on/off (1/0) sbeg heat clim. tendency
      rlc_sbei=0.               ! turn on/off (1/0) sbei heat clim. tendency
      rlc_sbes=0.               ! turn on/off (1/0) sbes heat clim. tendency
/

! Details about the vertical layer information (from the critical parameters)

! Momentum levels (note that some of the upper levels are encoded)
! Empty values are present since the code is writen such that there
! is current a maximum array size of 100 for the level information
&CANAM_LEVEL_MOMENTUM
  g(1)=-4810,
  g(2)=-3113,
  g(3)=-3159,
  g(4)=-3222,
  g(5)=-3311,
  g(6)=-3436,
  g(7)=-3610,
  g(8)=-3854,
  g(9)=-2120,
  g(10)=-2167,
  g(11)=-2234,
  g(12)=-2328,
  g(13)=-2459,
  g(14)=-2643,
  g(15)=-2901,
  g(16)=-1125,
  g(17)=-1174,
  g(18)=-1239,
  g(19)=-1327,
  g(20)=-1445,
  g(21)=-1602,
  g(22)=-1809,
  g(23)=-0108,
  g(24)=-0144,
  g(25)=-0190,
  g(26)=-0250,
  g(27)=-0327,
  g(28)=-0426,
  g(29)=-0550,
  g(30)=-0708,
  g(31)=-0904,
  g(32)=11,
  g(33)=15,
  g(34)=18,
  g(35)=23,
  g(36)=28,
  g(37)=35,
  g(38)=43,
  g(39)=52,
  g(40)=64,
  g(41)=77,
  g(42)=92,
  g(43)=110,
  g(44)=131,
  g(45)=154,
  g(46)=181,
  g(47)=211,
  g(48)=245,
  g(49)=283,
  g(50)=325,
  g(51)=370,
  g(52)=420,
  g(53)=474,
  g(54)=531,
  g(55)=592,
  g(56)=648,
  g(57)=698,
  g(58)=742,
  g(59)=780,
  g(60)=813,
  g(61)=841,
  g(62)=865,
  g(63)=885,
  g(64)=902,
  g(65)=916,
  g(66)=930,
  g(67)=944,
  g(68)=958,
  g(69)=972,
  g(70)=986,
  g(71)=995,
  g(72)=0,
  g(73)=0,
  g(74)=0,
  g(75)=0,
  g(76)=0,
  g(77)=0,
  g(78)=0,
  g(79)=0,
  g(80)=0,
  g(81)=0,
  g(82)=0,
  g(83)=0,
  g(84)=0,
  g(85)=0,
  g(86)=0,
  g(87)=0,
  g(88)=0,
  g(89)=0,
  g(90)=0,
  g(91)=0,
  g(92)=0,
  g(93)=0,
  g(94)=0,
  g(95)=0,
  g(96)=0,
  g(97)=0,
  g(98)=0,
  g(99)=0,
  g(100)=0,
/

! Thermodynamic levels (note that some of the upper levels are encoded)
&CANAM_LEVEL_THERMO
  h(1)=-4810,
  h(2)=-3113,
  h(3)=-3159,
  h(4)=-3222,
  h(5)=-3311,
  h(6)=-3436,
  h(7)=-3610,
  h(8)=-3854,
  h(9)=-2120,
  h(10)=-2167,
  h(11)=-2234,
  h(12)=-2328,
  h(13)=-2459,
  h(14)=-2643,
  h(15)=-2901,
  h(16)=-1125,
  h(17)=-1174,
  h(18)=-1239,
  h(19)=-1327,
  h(20)=-1445,
  h(21)=-1602,
  h(22)=-1809,
  h(23)=-0108,
  h(24)=-0144,
  h(25)=-0190,
  h(26)=-0250,
  h(27)=-0327,
  h(28)=-0426,
  h(29)=-0550,
  h(30)=-0708,
  h(31)=-0904,
  h(32)=11,
  h(33)=15,
  h(34)=18,
  h(35)=23,
  h(36)=28,
  h(37)=35,
  h(38)=43,
  h(39)=52,
  h(40)=64,
  h(41)=77,
  h(42)=92,
  h(43)=110,
  h(44)=131,
  h(45)=154,
  h(46)=181,
  h(47)=211,
  h(48)=245,
  h(49)=283,
  h(50)=325,
  h(51)=370,
  h(52)=420,
  h(53)=474,
  h(54)=531,
  h(55)=592,
  h(56)=648,
  h(57)=698,
  h(58)=742,
  h(59)=780,
  h(60)=813,
  h(61)=841,
  h(62)=865,
  h(63)=885,
  h(64)=902,
  h(65)=916,
  h(66)=930,
  h(67)=944,
  h(68)=958,
  h(69)=972,
  h(70)=986,
  h(71)=995,
  h(72)=0,
  h(73)=0,
  h(74)=0,
  h(75)=0,
  h(76)=0,
  h(77)=0,
  h(78)=0,
  h(79)=0,
  h(80)=0,
  h(81)=0,
  h(82)=0,
  h(83)=0,
  h(84)=0,
  h(85)=0,
  h(86)=0,
  h(87)=0,
  h(88)=0,
  h(89)=0,
  h(90)=0,
  h(91)=0,
  h(92)=0,
  h(93)=0,
  h(94)=0,
  h(95)=0,
  h(96)=0,
  h(97)=0,
  h(98)=0,
  h(99)=0,
  h(100)=0,
/
